# Frontend-Angular-Project

Feito com carinho por @vitorchumbinho123 :heart:

## Passo a passo  

   1. [Clonar o repositorio](#clonar-o-repositorio)
   2. [Criar a imagem docker](#criar-imagem-docker)
   3. [Executar imagem docker](#executar-imagem-docker)
   4. [Tudo Pronto](#tudo-pronto)

<br>

## Clonar o repositorio
Para clonar o repositorio, copie e cole o seguinte comando em seu termina.

```
$ git clone https://gitlab.com/vitorchumbinho123/frontend-angular-project.git
```

<br>

## Criar imagem docker

Abra o terminal dentro da pasta do projeto, cole o codigo abaixo e guarde a imagem ser gerada.

```
$ docker build -t frontend-angular .
```

<br>

## Executar imagem docker

Com o terminal ainda aberto, execute o comando abaixo para executar o container docker.

```
$ docker run -d -p 8088:88 frontend-angular
```

<br>

## Tudo pronto

Agora abra o seu navegador e digite a seguinte URL http://localhost:8088.

Essa aplicação funcionara em conjunto com a api que esta neste repositorio https://gitlab.com/vitorchumbinho123/backend-laravel-project.
