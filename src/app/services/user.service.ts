import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { API_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private isLoggedIn = new BehaviorSubject<boolean>(false)

  constructor(private http: HttpClient) { }

  //Toggle loggied in
  public toggleLogin(state: boolean): void {
    this.isLoggedIn.next(state)
  }
  //Get all users
  public getAll(): Observable<any> {
    const user: any = this.getLocalData()
    const userObj = JSON.parse(user)
    const token = userObj.user.token
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token
    })

    return this.http.get(API_URL, { headers: headers })
  }

  //
  public status() {
    const localData: any = this.getLocalData()
    if (!localData) {
      this.isLoggedIn.next(false)
      console.log("User not logged in!!")
    } else {
      this.isLoggedIn.next(true)
    }
    return this.isLoggedIn.asObservable()
  }

  //Login
  public login(email: string, password: string): Observable<any> {
    return this.http.post(API_URL + "/login", {
      email: email,
      password: password
    })
  }

  //Logout
  public logout(): void {
    const user: any = this.getLocalData()
    const userObj = JSON.parse(user)

    const token = userObj.user.token
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token
    })

    this.http.post(API_URL + "/logout", { headers: headers })
  }

  //Sign-Up
  public signUp(name: string, email: string, password: string): Observable<any> {
    return this.http.post(API_URL + "/sign-up", {
      "name": name,
      "email": email,
      "password": password
    })
  }
  //Update
  public update(name: string, email: string, password: string): Observable<any> {
    const user: any = this.getLocalData()
    const userObj = JSON.parse(user)

    const token = userObj.user.token
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token
    })

    return this.http.put(API_URL + "/update", {
      "name": name,
      "email": email,
      "password": password
    }, { headers: headers })
  }

  //local data
  public getLocalData() {
    const localData: any = localStorage.getItem("user")
    return localData
  }

  public getMyDatas(): Observable<any> {
    const user: any = this.getLocalData()
    const userObj = JSON.parse(user)
    const token = userObj.user.token
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token
    })

    return this.http.get(API_URL + "/show", { headers: headers })
  }

  public delete(id: number) {
    const user: any = this.getLocalData()
    const userObj = JSON.parse(user)
    const token = userObj.user.token
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token
    })
    return this.http.delete(API_URL + "/delete/" + id, { headers: headers })
  }
}

