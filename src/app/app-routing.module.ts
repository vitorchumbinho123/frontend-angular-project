import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { MyDatasComponent } from './components/my-datas/my-datas.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { UpdateMyDatasComponent } from './components/update-my-datas/update-my-datas.component';
import { UsersComponent } from './components/users/users.component';

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "logout", component: LogoutComponent },
  { path: "sign-up", component: SignUpComponent },
  { path: "users", component: UsersComponent },
  { path: "my-datas", component: MyDatasComponent },
  { path: "my-datas/update", component: UpdateMyDatasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
