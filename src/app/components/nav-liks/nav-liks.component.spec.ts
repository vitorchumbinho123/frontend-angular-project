import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavLiksComponent } from './nav-liks.component';

describe('NavLiksComponent', () => {
  let component: NavLiksComponent;
  let fixture: ComponentFixture<NavLiksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavLiksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavLiksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
