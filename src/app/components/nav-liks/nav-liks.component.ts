import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-nav-liks',
  templateUrl: './nav-liks.component.html',
  styleUrls: ['./nav-liks.component.css']
})
export class NavLiksComponent implements OnInit {
  loggedIn: boolean = false

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.status().subscribe(res => {
      this.loggedIn = res
    })
  }

}
