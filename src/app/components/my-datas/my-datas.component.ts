import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-my-datas',
  templateUrl: './my-datas.component.html',
  styleUrls: ['./my-datas.component.css']
})
export class MyDatasComponent implements OnInit {
  isLogged: boolean = false
  myDatas: any
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.status().subscribe(res => { this.isLogged = res })
    this.getMyDatas()
  }

  getMyDatas(): void {
    this.userService.getMyDatas().subscribe({
      next: (res: any) => {
        this.myDatas = res.user
      }

    })
  }
}
