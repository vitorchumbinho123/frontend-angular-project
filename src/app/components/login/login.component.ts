import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private userCervice: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  authenticate(form: NgForm): void {
    const email = form.value.email
    const password = form.value.password

    this.userCervice.login(email, password).subscribe({
      next: (user: any) => {
        localStorage.setItem("user", JSON.stringify(user))
        this.router.navigate(["/users"])
      }
    })
  }
}
