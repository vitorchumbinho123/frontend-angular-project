import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: any
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.userService.status().subscribe(res => {
      console.log(res)
    })
    console.log(this.getAllUsers())
  }

  getAllUsers() {
    const userLocalStorage: any = this.userService.getLocalData()
    const userLogged = JSON.parse(userLocalStorage)
    this.userService.getAll().subscribe({
      next: (users: any) => {
        this.users = users.users
      }
    })
  }

  deleteUser(id: number) {
    this.userService.delete(id).subscribe()
    this.getAllUsers()
  }
}

