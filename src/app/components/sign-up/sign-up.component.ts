import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  signUpUser(form: NgForm): void {
    const name = form.value.name
    const email = form.value.email
    const password = form.value.password

    this.userService.signUp(name, email, password).subscribe(
      {
        next: (res: any) => {
          alert(res["message"])
        }
      }
    )
  }

}
