import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMyDatasComponent } from './update-my-datas.component';

describe('UpdateMyDatasComponent', () => {
  let component: UpdateMyDatasComponent;
  let fixture: ComponentFixture<UpdateMyDatasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMyDatasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMyDatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
