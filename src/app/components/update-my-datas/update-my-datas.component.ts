import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-my-datas',
  templateUrl: './update-my-datas.component.html',
  styleUrls: ['./update-my-datas.component.css']
})
export class UpdateMyDatasComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  updateMyDatas(form: NgForm) {
    const name = form.value.name
    const email = form.value.email
    const password = form.value.password

    this.userService.update(name, email, password).subscribe(
      {
        next: (res: any) => {
          this.router.navigate(["/my-datas"])
          let oldData = JSON.parse(this.userService.getLocalData())
          oldData.name = res.name
          oldData.email = res.email
          localStorage.setItem("user",JSON.stringify(oldData))
        }
      }
    )
  }

}
